<?php
class Home extends Controller {

    public function __construct()
    {
        if(!isset($_SESSION['login'])){
            header('Location: ' . BASEURL . '/login');
        }
    }

    public function index()
    {
        //encho "Home/index";
        $data['judul'] = 'Daftar siswa';
        $data['siswa'] = $this->model('Siswa_model')->getAllSiswa();
        $data["nama"] = $this->model("User_model")->getUser();
        $this->view('template/header', $data);
        $this->view('home/index',$data,);
        $this->view('template/footer');
    }

    public function logout(){
        session_destroy();
        header('Location: ' . BASEURL . '/login');
        exit;
    }
}


