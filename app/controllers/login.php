<?php

class login extends Controller{

    public function __construct()
    {
        if(isset($_SESSION['login'])){
            header('Location: ' . BASEURL . '/home');
        }
    }

    public function index(){
        $this->view('login/index');
    }

    public function prosesLogin(){
        if($this->model('User_model')->login($_POST) > 0){
            $_SESSION['login'] = true;
            header('Location: ' . BASEURL . '/home');
        }else{
            echo "Password salah" ;
        }
    }

    public function coba(){
        echo "hai";
    }

    public function logout(){
        session_destroy();

        header('Location: http://localhost/PWPB/public/login');
        exit;
    }

}
