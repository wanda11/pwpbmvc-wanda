<?php

class User_model {
    private $table = 'users';
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }
    
    private $nama = 'wanda';

    public function getUser()
    {
        return $this->nama;
    }

    public function register($data){
        $email = $data['email'];
        $password = $data['password'];
        $cpassword = $data['cpassword'];
        
        if($password !== $cpassword){
            echo "tidak match";
        }
        $passwordHashed = password_hash($password, PASSWORD_DEFAULT); 
        $query = "INSERT INTO users (email, password) VALUES (:email, :password)";
        $this->db->query($query);
        $this->db->bind('email', $email);
        $this->db->bind('password', $passwordHashed);
        $this->db->execute();
        return 1;
    }

    public function login($data){
        $email = $data['email'];
        $passwordPost = $data['password'];

        $queryGetUser = "SELECT * FROM users WHERE email = :email";
        $this->db->query($queryGetUser);
        $this->db->bind('email', $email);
        $user = $this->db->single();
        $passwordUser = $user['password'];

        if (password_verify($passwordPost, $passwordUser)) {
            return 1;
        }else{
            return -1;
        }
        
    }
}