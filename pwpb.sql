-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 30, 2023 at 07:25 AM
-- Server version: 8.0.30
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pwpb`
--

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id` int NOT NULL,
  `nama` varchar(255) NOT NULL,
  `nis` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `jurusan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id`, `nama`, `nis`, `email`, `jurusan`) VALUES
(1, 'wanda', '29699', 'wanda@gmail.com', 'Teknik Pendingin'),
(2, 'Ulan', '29690', 'ulan@gmail.com', 'Teknik Permesinan'),
(3, 'cindy', '29691', 'cindy@gmail.com', 'Teknik Pendingin');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`) VALUES
(1, 'wandapridayanti@gmail.com', 'wanda123'),
(2, 'ulanpratiwi09@gmail.com', '$2y$10$h1nSyEcV2wLJay8wHc3YH.o2tcnnPbhDPxdmnwMvL4G8gEzC8LtyC'),
(3, 'sena@gmail.com', '$2y$10$ASpgCW4/qJxOscyi1CbLuORQ7V.c8PQW4tfzXvQamAvS/KESbjlH6'),
(4, 'sena@gmail.com', '$2y$10$azU1rHv1FuRVgEvflApd0./UfNVUMDqCZz8XAKMXpOStSVXIRZRr6'),
(5, 'sena@gmail.com', '$2y$10$lUjkBhg6OWsVP0eULvnyXeJccI6ab3Xq9Vv1ATYo2wWAwKNluMlQ.'),
(6, 'wanda@gmail.com', '$2y$10$LgY6n/1BbhH3oCQga78wcuYV1wT1z.90wam8PyhgJCVnSXpmQdEoa'),
(7, 'radika@gmail.com', '$2y$10$ZV2AcZ0.JH62mvXlLGD0JeHTr.EdKQUK.XjKIg82JFvfF3mcJ7f0e'),
(8, 'wanda@gmail.com', '$2y$10$LG56F0Ls06a7YVfVQ2PiNOM3AzqJJLhlbxZTF1Su9oQEEQLiL7NMC');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
